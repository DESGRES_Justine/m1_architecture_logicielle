<?php 
    require('controller/controller.php'); 

    use Auth0\SDK\Auth0;
    use Auth0\SDK\Configuration\SdkConfiguration;
    use Auth0\SDK\Utility\HttpResponse;

    if(!isset($auth0)){
        // Import the Composer Autoloader to make the SDK classes accessible:
        require 'vendor/autoload.php';

        // Load our environment variables from the .env file:
        (Dotenv\Dotenv::createImmutable(__DIR__))->load();

        // Now instantiate the Auth0 class with our configuration:
        $auth0 = new Auth0([
            'domain' => $_ENV['AUTH0_DOMAIN'],
            'clientId' => $_ENV['AUTH0_CLIENT_ID'],
            'clientSecret' => $_ENV['AUTH0_CLIENT_SECRET'],
            'cookieSecret' => $_ENV['AUTH0_COOKIE_SECRET']
        ]);
    }
    // Define route constants:
    define('ROUTE_URL_INDEX', rtrim($_ENV['AUTH0_BASE_URL'], '/'));
    define('ROUTE_URL_LOGIN', ROUTE_URL_INDEX . 'login');
    define('ROUTE_URL_CALLBACK', ROUTE_URL_INDEX . 'callback');
    define('ROUTE_URL_LOGOUT', ROUTE_URL_INDEX . 'logout');


    if(isset($_GET['login'])){
        login($auth0,ROUTE_URL_CALLBACK);
    }elseif(isset($_GET['callback'])){
        callback($auth0,ROUTE_URL_CALLBACK,ROUTE_URL_INDEX);
    }elseif(isset($_GET['logout'])){
        logout($auth0,ROUTE_URL_INDEX);
    }else{

        isConnect($auth0);
        if(isset($_GET['keyword'])){
            listFeels($_GET['keyword']);
        } elseif(isset($_GET['add'])){
            if(isset($_SESSION['user']) && $_SESSION['user'] != null){
                require('view/viewAddFeel.php');
            }else{
                header('location:' . ROUTE_URL_LOGIN);
            }
        }elseif(isset($_POST['send'])){
            sendFeel($_SESSION['user']->user['nickname'],$_POST['avis'],$_POST['rating'],$_POST['tag']);
        }elseif(isset($_GET['account']) && isset($_SESSION['user'])){
            authorFeels($_SESSION['user']->user['nickname']);
        }else{
            loadHome();
        }
    }


    

