<?php ob_start(); ?>
    
<div class="row w-100">
    <div class="col justify-content-center">
        <div class="display-4 text-center my-5" style="font-size: 3em;">Vos avis</div>
        <div class="w-75 offset-1">
            <?php
            if($feels != null ){
                foreach($feels as $feel){
                    echo "<div class='card m-2 p-2'><div class='row'>
                              <div class='col-8 mt-1 ml-2'>
                                    <div class='display-4' style='font-size: 1.5em; font-weight: 500;'>". $feel->author ."</div>
                              </div>
                              <div class='col-3 mt-1 text-right'>
                                    <div class='display-4' style='font-size: 1.5em;'>Note : ". $feel->rating ."/5</div>
                              </div>
                          </div>";
                    echo "<div class='font-italic ml-2'>". $feel->date ."</div>";
                    echo "<hr/>";
                    echo "<div class='mb-3 ml-2'>". $feel->comment ."</div></div>";
                }
            }
            ?>
        </div>
    </div>

    <div class="col">
        <div class="display-4 text-center my-5" style="font-size: 3em;">Informations personnelles</div>
        <div class="row mb-3">
            <div class="col-3">
                <div class="display-4" style="font-size: 1.5em;">Pseudo : </div>
            </div>
            <div class="col-8">
                <div class="h5"><?php echo $_SESSION['user']->user['nickname']; ?></div>
            </div>
        </div>
        <div class="row">
            <div class="col-3">
                <div class="display-4" style="font-size: 1.5em;">Email :</div>
            </div>
            <div class="col-8">
                <div class="h5"><?php echo $_SESSION['user']->user['email']; ?></div>
            </div>
        </div>
    </div>
</div>

<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>
 