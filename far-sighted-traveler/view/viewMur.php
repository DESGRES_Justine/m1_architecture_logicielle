
<?php ob_start(); ?>
    
<div class="row justify-content-center w-100">
        <?php
            if(!is_null($feels)){
                foreach($feels as $feel){
                    echo "<div class='card w-75 m-2 p-2'>
                                <div class='row'>
                                    <div class='col-8 mt-1 ml-2'>
                                        <div class='display-4' style='font-size: 1.5em; font-weight: 500;'>". $feel->author ."</div>
                                    </div>
                                    <div class='col-3 mt-1 text-right'>
                                        <div class='display-4' style='font-size: 1.5em;'>Note : ". $feel->rating ."/5</div>
                                    </div>
                                </div>";
                    echo "<div class='font-italic ml-2'>". $feel->date ."</div>";
                    echo "<hr/>";
                    echo "<div class='mb-3 ml-2'>". $feel->comment ."</div></div>";
                }
            }else{
                echo "<div class='display-4 mt-3' style='font-size:2em;'>Aucun avis trouvé</div>";
            }
        ?>
</div>

<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>
