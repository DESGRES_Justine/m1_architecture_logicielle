<?php ob_start(); ?>
    <div class="row" style="height: 88vh; width:100vw;">
        <div class="col">
            <div id='map' class="w-100 h-100"></div>
        </div>
        <div class="col" id ="infoColumn" style="margin-top: 10%">
            <div class="card bg-secondary m-2 p-2">
                <div class="card-title h2 text-center text-white text-uppercase display-3"><label id="city"><label></div>
                <div class="card-subtitle h4 mb-2 text-white text-center display-4"><label id='rating'>-</label>/5 (<label id='ratingCount'></label> avis)</div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item rounded">
                        <div class="row">
                            <div class="col text-uppercase">Température</div>
                            <div class="col"><div class="d-inline-block" id="temp"></div>°C</div>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="row">
                            <div class="col text-uppercase">Région</div>
                            <div class="col" id = "region"></div>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="row">
                            <div class="col text-uppercase">Département</div>
                            <div class="col" id ="departement"></div>
                        </div>
                    </li>
                </ul>
            </div>
            <pre id="coordinates" class="coordinates"></pre>
            <div id='info'></div>
        </div>
    </div>

    <?php echo $script ?>

<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>
