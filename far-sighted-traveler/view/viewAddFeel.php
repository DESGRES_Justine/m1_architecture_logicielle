<?php ob_start(); ?>
<div class="row justify-content-center w-100">
    <div class="card" style="width: 50vw !important; margin-top: 4%; background-color: #F6F6F6;">
        <div class="card-header display-3 text-center">Ajouter un avis</div>
        <form action="index.php" method="post" class="col">
            <div class="form-row my-4">
                <label for="name" class="col-3 form-label display-4" style="font-size: 2em;">Pseudo :</label>
                <input class="col-9 form-control" type="text" placeholder="<?php echo $_SESSION['user']->user['nickname'] ?>" readonly>
            </div>
            <div class="form-row mb-4">
                <label for="avis" class="col-3 form-label display-4" style="font-size: 2em;">Avis :</label>
                <textarea id="avis" class="col-9 form-control" name="avis" placeholder="Ecrivez votre avis ici"  rows="3" required></textarea>
            </div>
            <div class="form-row mb-4">
                <label for="tag" class="col-3 form-label display-4" style="font-size: 2em;">Tag :</label>
                <input type="text" id="tag" class="col-9 form-control" name="tag" placeholder="ex : Paris" required>
            </div>
            <div class="form-row mb-4">
                <label for="rating" class="col-3 form-label display-4" style="font-size: 2em;">Note :</label>
                <label class="h5 mx-2 mt-2">0</label><input type="range" id="rating" class="col-8 form-range" name="rating" min="0" max="5" step="1" required><label class="h5 ml-3 mt-2">5</label>
            </div>
            <button class="btn btn-outline-success my-2 offset-5 display-4" type="submit" name="send" style="font-size: 1.5em;">Publier</button>
        </form>
    </div>
</div>


<?php $content = ob_get_clean(); ?>
<?php require('template.php'); ?>