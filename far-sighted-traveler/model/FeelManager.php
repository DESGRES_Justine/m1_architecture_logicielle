<?php

class  FeelManager {

    public function getfeels($key){

        $response = file_get_contents('http://far-sighted-traveler.local/API/feels/'. $key);

        return json_decode($response);
    }

    public function postfeel($author, $com, $rate, $tag){

        $feel = array('author' => $author, 'com' => $com, 'rate' => $rate, 'tag' => $tag );
        $options = array(
                'http' => array(
                'method'  => 'POST',
                'header'  => "Content-Type: application/x-www-form-urlencoded",
                'content' => http_build_query($feel),
            )
        );

        $context  = stream_context_create($options);

        $response = file_get_contents('http://far-sighted-traveler.local/API/addfeel', false, $context);

        return $response;
    }

    public function getauthorfeels($author){
        
        $response = file_get_contents('http://far-sighted-traveler.local/API/authorFeels/'. $author);

        return json_decode($response);
    }

}