<?php 

  require('model/FeelManager.php');

/********************* API Feel ***********************/

  function listFeels($data){
    
    $feelManager = new FeelManager();
    $feels = $feelManager->getfeels($data);

    require('view/viewMur.php');

  }

  function authorFeels($author){
    $feelManager = new FeelManager();
    $feels = $feelManager->getauthorfeels($author);

    require('view/viewAccount.php');

  }

  function sendFeel($author, $com, $rate, $tag){

    $feelManager = new FeelManager();
    $postedFeel = $feelManager->postfeel($author,$com,$rate,$tag);

    if ($postedFeel === false) {
      throw new Exception('Impossible d\'ajouter l\'avis !');
    }else{
      $script = loadScriptJSHome();
      require('view/viewMap.php');
    }

  }

  function loadScriptJSHome(){
    return '<script src="../far-sighted-traveler/API/APImapbox.js" ></script><script src="../far-sighted-traveler/API/APIpositionstack.js" ></script><script src="../far-sighted-traveler/API/APIfeels.js" ></script><script src="../far-sighted-traveler/API/APIaddress.js" ></script><script src="../far-sighted-traveler/API/APImeteo.js" ></script>';
  }

  function loadHome(){
    $script = loadScriptJSHome();
    require('view/viewMap.php');
  }




  /********************* API Auth0 ***********************/

  function login($auth0,$callback){
    // Reset user sessions each time they go to login to avoid "invalid state" errors, should they hit network issues or other problems that interrupt a previous login process:
    $auth0->clear();
    // Finally, set up the local application session, and redirect the user to the Auth0 Universal Login Page to authenticate.
    header("Location: " . $auth0->login($callback));
    exit;
  }

  function callback($auth0,$callback,$index){
    // Have the SDK complete the authentication flow:
    $auth0->exchange($callback);

    // Finally, redirect our end user back to the / index route, to display their user profile:
    header("Location: " . $index);
    exit;
  }

  function logout($auth0,$index){
    // Clear the user's local session with our app, then redirect them to the Auth0 logout endpoint to clear their Auth0 session.
    header("Location: " . $auth0->logout($index));
    exit;
  }

  function isConnect($auth0){
    $_SESSION['user'] = $auth0->getCredentials();
  }


