/************* APImeteo **************/


async function getMeteo() {
   
    try{
        let url = "https://api.open-meteo.com/v1/forecast?" + "latitude=" + map.getLat() + "&longitude="+ map.getLng() + "&current_weather=true&timezone=Europe%2FBerlin";
        let res = await fetch(url);
        let infos = await res.json();
        let infosProcessed = infos['current_weather']['temperature'];

        return infosProcessed;

    } catch (error) {
        console.log(error);
    }
}

async function dispMeteo(){
        let meteoInfo = await getMeteo();
        document.getElementById("temp").innerText = meteoInfo;
   
}