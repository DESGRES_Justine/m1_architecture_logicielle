const MAPBOXTOKEN = "pk.eyJ1IjoianVzdGluZWRlc2dyZXMiLCJhIjoiY2t3bW4xb245MTR5ajMxbXQ5aXdpb243ZSJ9.hZoDrn50bupZi2oCYxpoCA";

class APIMapBox {

    constructor(){
        this.defaultLong = 2.626380;
        this.defaultlat = 47.000000;

        this.initialiseMap("map",null,null)
        this.initialiseMarker();
        this.getMarker().on('dragend', (e) => {
            this.onDragEnd()
        });
    }

    initialiseMap(containerId, long , lat){

        // Display of a map

        mapboxgl.accessToken = MAPBOXTOKEN;
        this.map = new mapboxgl.Map({
            container: containerId,
            style: 'mapbox://styles/mapbox/streets-v11',
            center : [this.defaultLong,this.defaultlat],
            zoom : 5
        });
    }

    initialiseMarker(){
        this.marker = new mapboxgl.Marker({
            draggable: true
        })
        .setLngLat([this.defaultLong, this.defaultlat])
        .addTo(this.map);

    }

    onDragEnd() {
        const lngLat = this.marker.getLngLat();
        this.defaultLong = lngLat.lng;
        this.defaultlat = lngLat.lat;

    }

    getLng(){
        return this.defaultLong
    }

    getLat(){
        return this.defaultlat
    }

    getMap(){
        return this.map
    }

    getMarker(){
        return this.marker
    }

}

const map = new APIMapBox();


 


