
async function getRating(key,destinationId1,destinationId2) {
    try{
        let url = "http://far-sighted-traveler.local/API/rate/" + key;
        let res = fetch(url);
        res.then((response) => response.json())
            .then((responseJSON) => {
                if(destinationId1 != null){
                    console.log(responseJSON);
                    if(responseJSON != null && responseJSON.length >= 1){
                        
                        let moy = 0;
                        for(let i = 0; i < responseJSON.length; i++){
                            moy += parseInt(responseJSON[i]);
                        }
                        document.getElementById(destinationId1).innerText = (moy / responseJSON.length).toFixed(1);
                        document.getElementById(destinationId2).innerText = responseJSON.length;
                    }else{
                        document.getElementById("rating").innerText = "-"; 
                        document.getElementById("ratingCount").innerText = 0;
                    }
                }
            });

    } catch (error) {
        console.log(error);
    }
}



