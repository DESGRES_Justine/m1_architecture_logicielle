/*************** API Adress ****************/

// Data processing
async function renderAdressInformations(propertyToLoad, destinationId) {
    try{
        let url = "https://api-adresse.data.gouv.fr/search/?";
        let res = await fetch(url);
        let infos = res.json();
        let infosProcessed = [];

        /*if(infos['features'][0] !== undefined){
            console.log(infos['features'][0]['properties']['city'] + ", " + infos['features'][0]['properties']['context']);
        }*/

        for(let i = 0; i < infos['features'].length; i++){
            infosProcessed[i] = infos['features'][i]['properties'][propertyToLoad];
        }

        // If the result have to be displayed on page
        if(destinationId !== undefined){
            let templateElement = document.getElementById(destinationId);
            templateElement.innerText = infosProcessed.toString();
        }

        return infosProcessed;

    } catch (error) {
        console.log(error);
    }
}

// Data reverse processing
async function renderAdressInformationsReverse(propertyToLoad) {

    try{
        let url = "https://api-adresse.data.gouv.fr/reverse/?" + "lon=" + map.getLng() + "&lat="+ map.getLat() ;
        let res = await fetch(url);
        let infos = await res.json();
        let infosProcessed = null;

        if(infos['features'][0] != undefined){
            infosProcessed = infos['features'][0]['properties'][propertyToLoad];
        }

        return infosProcessed;

    } catch (error) {
        console.log(error);
    }
}

map.getMarker().on('dragend', (e) => {
    disp();
    dispMeteo();
});

async function disp(){
        let cityInfo = await renderAdressInformationsReverse("city");
        console.log(cityInfo);
        if(cityInfo != null){
            document.getElementById("city").innerText= cityInfo;
            let contextInfo = await renderAdressInformationsReverse("context");
            let splitContextInfo = contextInfo.split(",");
            document.getElementById("region").innerText = splitContextInfo[2];
            document.getElementById("departement").innerText = splitContextInfo[1] + " " + splitContextInfo[0];
            getRating(cityInfo,"rating","ratingCount");
            document.getElementById("region").parentNode.parentNode.hidden = false;
            document.getElementById("departement").parentNode.parentNode.hidden = false;
        }else{
            document.getElementById("city").innerText= "";
            document.getElementById("region").innerText = "";
            document.getElementById("departement").innerText ="";
            let departInfo = await getDep("region");
            document.getElementById("city").innerText= departInfo;
            getRating(departInfo,"rating","ratingCount");
            document.getElementById("region").parentNode.parentNode.hidden = true;
            document.getElementById("departement").parentNode.parentNode.hidden = true;
        }
}





