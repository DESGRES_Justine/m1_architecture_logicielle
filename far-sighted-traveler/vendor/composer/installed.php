<?php return array(
    'root' => array(
        'pretty_version' => 'dev-main',
        'version' => 'dev-main',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => '77465f3ff44a33b9bbdc2f9e58bda7eb192fc6b7',
        'name' => '__root__',
        'dev' => true,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => '77465f3ff44a33b9bbdc2f9e58bda7eb192fc6b7',
            'dev_requirement' => false,
        ),
        'auth0/auth0-php' => array(
            'pretty_version' => '8.0.3',
            'version' => '8.0.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../auth0/auth0-php',
            'aliases' => array(),
            'reference' => '3749db6b6d8690f25e04bc3cbd2b1afccccaf353',
            'dev_requirement' => false,
        ),
        'graham-campbell/result-type' => array(
            'pretty_version' => 'v1.0.4',
            'version' => '1.0.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../graham-campbell/result-type',
            'aliases' => array(),
            'reference' => '0690bde05318336c7221785f2a932467f98b64ca',
            'dev_requirement' => false,
        ),
        'kriswallsmith/buzz' => array(
            'pretty_version' => '1.2.0',
            'version' => '1.2.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../kriswallsmith/buzz',
            'aliases' => array(),
            'reference' => 'e7468d13f33fb6656068372533f2a446602fef09',
            'dev_requirement' => false,
        ),
        'nyholm/psr7' => array(
            'pretty_version' => '1.4.1',
            'version' => '1.4.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nyholm/psr7',
            'aliases' => array(),
            'reference' => '2212385b47153ea71b1c1b1374f8cb5e4f7892ec',
            'dev_requirement' => false,
        ),
        'php-http/client-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'php-http/discovery' => array(
            'pretty_version' => '1.14.1',
            'version' => '1.14.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../php-http/discovery',
            'aliases' => array(),
            'reference' => 'de90ab2b41d7d61609f504e031339776bc8c7223',
            'dev_requirement' => false,
        ),
        'php-http/httplug' => array(
            'pretty_version' => '2.2.0',
            'version' => '2.2.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../php-http/httplug',
            'aliases' => array(),
            'reference' => '191a0a1b41ed026b717421931f8d3bd2514ffbf9',
            'dev_requirement' => false,
        ),
        'php-http/message-factory' => array(
            'pretty_version' => 'v1.0.2',
            'version' => '1.0.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../php-http/message-factory',
            'aliases' => array(),
            'reference' => 'a478cb11f66a6ac48d8954216cfed9aa06a501a1',
            'dev_requirement' => false,
        ),
        'php-http/multipart-stream-builder' => array(
            'pretty_version' => '1.2.0',
            'version' => '1.2.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../php-http/multipart-stream-builder',
            'aliases' => array(),
            'reference' => '11c1d31f72e01c738bbce9e27649a7cca829c30e',
            'dev_requirement' => false,
        ),
        'php-http/promise' => array(
            'pretty_version' => '1.1.0',
            'version' => '1.1.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../php-http/promise',
            'aliases' => array(),
            'reference' => '4c4c1f9b7289a2ec57cde7f1e9762a5789506f88',
            'dev_requirement' => false,
        ),
        'phpoption/phpoption' => array(
            'pretty_version' => '1.8.0',
            'version' => '1.8.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpoption/phpoption',
            'aliases' => array(),
            'reference' => '5455cb38aed4523f99977c4a12ef19da4bfe2a28',
            'dev_requirement' => false,
        ),
        'psr/cache' => array(
            'pretty_version' => '3.0.0',
            'version' => '3.0.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/cache',
            'aliases' => array(),
            'reference' => 'aa5030cfa5405eccfdcb1083ce040c2cb8d253bf',
            'dev_requirement' => false,
        ),
        'psr/event-dispatcher' => array(
            'pretty_version' => '1.0.0',
            'version' => '1.0.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/event-dispatcher',
            'aliases' => array(),
            'reference' => 'dbefd12671e8a14ec7f180cab83036ed26714bb0',
            'dev_requirement' => false,
        ),
        'psr/http-client' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-client',
            'aliases' => array(),
            'reference' => '2dfb5f6c5eff0e91e20e913f8c5452ed95b86621',
            'dev_requirement' => false,
        ),
        'psr/http-client-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/http-factory' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-factory',
            'aliases' => array(),
            'reference' => '12ac7fcd07e5b077433f5f2bee95b3a771bf61be',
            'dev_requirement' => false,
        ),
        'psr/http-factory-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/http-message' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-message',
            'aliases' => array(),
            'reference' => 'f6561bf28d520154e4b0ec72be95418abe6d9363',
            'dev_requirement' => false,
        ),
        'psr/http-message-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'symfony/deprecation-contracts' => array(
            'pretty_version' => 'v3.0.0',
            'version' => '3.0.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/deprecation-contracts',
            'aliases' => array(),
            'reference' => 'c726b64c1ccfe2896cb7df2e1331c357ad1c8ced',
            'dev_requirement' => false,
        ),
        'symfony/options-resolver' => array(
            'pretty_version' => 'v5.4.0',
            'version' => '5.4.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/options-resolver',
            'aliases' => array(),
            'reference' => 'b0fb78576487af19c500aaddb269fd36701d4847',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-ctype' => array(
            'pretty_version' => 'v1.23.0',
            'version' => '1.23.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-ctype',
            'aliases' => array(),
            'reference' => '46cd95797e9df938fdd2b03693b5fca5e64b01ce',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-mbstring' => array(
            'pretty_version' => 'v1.23.1',
            'version' => '1.23.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-mbstring',
            'aliases' => array(),
            'reference' => '9174a3d80210dca8daa7f31fec659150bbeabfc6',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php73' => array(
            'pretty_version' => 'v1.23.0',
            'version' => '1.23.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php73',
            'aliases' => array(),
            'reference' => 'fba8933c384d6476ab14fb7b8526e5287ca7e010',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php80' => array(
            'pretty_version' => 'v1.23.1',
            'version' => '1.23.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php80',
            'aliases' => array(),
            'reference' => '1100343ed1a92e3a38f9ae122fc0eb21602547be',
            'dev_requirement' => false,
        ),
        'vlucas/phpdotenv' => array(
            'pretty_version' => 'v5.4.0',
            'version' => '5.4.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../vlucas/phpdotenv',
            'aliases' => array(),
            'reference' => 'd4394d044ed69a8f244f3445bcedf8a0d7fe2403',
            'dev_requirement' => false,
        ),
    ),
);
