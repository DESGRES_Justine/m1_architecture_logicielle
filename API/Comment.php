<?php

class Comment implements JsonSerializable{

    private $_id;
    private $_comment;
    private $_author;
    private $_keyword;
    private $_date;
    private $_rate;

    function __construct($id,$author,$comment,$keyword,$date,$rate){
        $this->_id = $id;
        $this->_comment = $comment;
        $this->_author = $author;
        $this->_keyword = $keyword;
        $this->_date = $date;
        $this->_rate = $rate;
    }
        
    public function id(){
        return $this->_id;
    }

    public function author(){
        return $this->_author;
    }

    public function keyword(){
        return $this->_keyword;
    }

    public function date(){
        return $this->_date;
    }

    public function comment($value = ""){

        if($value == ""){
            return $this->_comment;
        }

        $this->_comment = $value;
    } 

    public function Rate(){
        return $this->_rate;
    }

    public function jsonSerialize()
    {
        return 
        [
            'id' => $this->_id,
            'author' =>$this->_author,
            'comment' =>$this->_comment,
            'keyword' =>$this->_keyword,
            'date' =>$this->_date,
            'rating' =>$this->_rate,
        ];
    }
}
?>