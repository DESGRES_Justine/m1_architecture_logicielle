<?php

require_once("Comment.php");

//Permet de récuperer tous les avis 
function getFeels(){
    $dataBase = dbConnect();

    //requete SQL
    $response = $dataBase->prepare('SELECT id,author,comment,keyword,DATE_FORMAT(comment_date,\'le %d/%m/%Y %Hh%imin%ss \') AS dateCommentaire,rating FROM post_user ORDER BY comment_date DESC');
    $response->execute();

    $listComment = null;

    //Reception des données extraite
    while($data = $response->fetch()){
        $listComment[] = new comment($data['id'], $data['author'],$data['comment'], $data['keyword'],$data['dateCommentaire'],$data['rating']);
    }
    $response->closeCursor();

    //envoye en JSON
    sendJSON($listComment);
}

//Permet de récuperer les avis par rapport à un mot clé
function getFeelsByKey($key){
    $dataBase = dbConnect();

    //requete SQL
    $response = $dataBase->prepare('SELECT id,author,comment,keyword,DATE_FORMAT(comment_date,\'le %d/%m/%Y à %Hh%i \') AS dateCommentaire,rating FROM post_user WHERE keyword = ? ORDER BY comment_date DESC');
    $response->execute(array($key));

    $listComment = null;

    //Reception des données extraite
    while($data = $response->fetch()){
        $listComment[] = new comment($data['id'], $data['author'],$data['comment'], $data['keyword'],$data['dateCommentaire'],$data['rating']);
    }
    $response->closeCursor();

    //envoye en JSON
    sendJSON($listComment);
}

//Permet de récuperer les avis par rapport à un auteur
function getFeelsByAuthor($key){
    $dataBase = dbConnect();

    //requete SQL
    $response = $dataBase->prepare('SELECT id,author,comment,keyword,DATE_FORMAT(comment_date,\'le %d/%m/%Y à %Hh%i \') AS dateCommentaire,rating FROM post_user WHERE author = ? ORDER BY comment_date');
    $response->execute(array($key));

    $listComment = null;

    //Reception des données extraite
    while($data = $response->fetch()){
        $listComment[] = new comment($data['id'], $data['author'],$data['comment'], $data['keyword'],$data['dateCommentaire'],$data['rating']);
    }
    $response->closeCursor();
    
    //envoye en JSON
    sendJSON($listComment);
}

//Permet de récuperer les notes dans les avis par rapport à un mot clé
function getRatingByKey($key){
    $dataBase = dbConnect();

    //requete SQL
    $response = $dataBase->prepare('SELECT rating FROM post_user WHERE keyword = ? ');
    $response->execute(array($key));

    $listRate = null;

    //Reception des données extraites
    while($data = $response->fetch()){
        $listRate[] = $data['rating'];
    }
    $response->closeCursor();

    //envoie en JSON
    sendJSON($listRate);

}

//Permet de récuperer un avis par rapport à son ID
function getFeelById($id){
    $dataBase = dbConnect();

    //requete SQL
    $response = $dataBase->prepare('SELECT id,author,comment,keyword,DATE_FORMAT(comment_date,\'le %d/%m/%Y à %Hh%i \') AS dateCommentaire,rating FROM post_user WHERE id = ?');
    $response->execute(array($id));

    //Reception des données extraite
    $data = $response->fetch();

    if(empty($data)){
        throw new Exception('Le post n\'existe pas!');
    }else{
        $comment = new comment($data['id'], $data['author'],$data['comment'], $data['keyword'],$data['dateCommentaire'],$data['rating']);
    }
    $response->closeCursor();
    
    //envoye en JSON
    sendJSON($comment);

}

//Permet de créer un nouveau avis dans la base de donnee
function postFeel($author,$comment,$rate,$tag){
    date_default_timezone_set("Europe/Paris");


    //On recupere la date du jour 
    $current_time = date("Y/m/d h:i");

    $dataBase = dbConnect();

    //requete SQL
    $requete = $dataBase->prepare('INSERT INTO post_user (id,author,comment,keyword,comment_date,rating) VALUES (NULL,:auteur,:commentaire,:tag,:cur_date,:rate)');
    $requete->execute(array(
        'auteur' => $author,
        'commentaire' => $comment,
        'rate' => $rate,
        'tag' => $tag,
        'cur_date' => $current_time
    ));
}

//Crée la connection avec la base de donnee
function dbConnect(){
    return new PDO('mysql:host=localhost;dbname=far-sighted-traveler;charset=utf8','root','' );
}

//affiche les data en format JSON
function sendJSON($data){
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json");
    if($data != null){
        echo json_encode($data,JSON_UNESCAPED_UNICODE);
    }else{
        echo "null";
    }
}
