<?php

require_once('api.php');

//www.monsite.fr/feels/:key
//www.monsite.fr/feels
//www.monsite.fr/feel/:id
//www.monsite.fr/addfeel
//www.monsite.fr/rate/:key
//www.monsite.fr/authorFeels/:author

//root l'url envoye et les data envoyé un POST
try{
    if(!empty($_GET['ask'])){
        $url = explode("/", filter_var($_GET['ask'],FILTER_SANITIZE_URL));
        switch($url[0]){
            case "feels" : 
                if(empty($url[1])){
                    getFeels();
                }else{
                    getFeelsByKey($url[1]);
                }
            break;
            case "feel" : 
                if(empty($url[1])){
                    throw new Exception("aucun id soumis");
                } else {
                    getFeelById($url[1]);
                }
            break;
            case "addfeel" : 
                if(isset($_POST['author']) && isset($_POST['com']) && isset($_POST['rate']) && isset($_POST['tag']) ) {
                    postFeel($_POST['author'], $_POST['com'], $_POST['rate'], $_POST['tag']);
                }else{
                    throw new Exception("Pas de post");
                }
            break;
            case "rate" : 
                if(!empty($url[1])) {
                    getRatingByKey($url[1]);
                }else{
                    throw new Exception("Probleme de mot clé");
                }
            break;
            case "authorFeels" : 
                if(!empty($url[1])) {
                    getFeelsByAuthor($url[1]);
                }else{
                    throw new Exception("Probleme pas d'auteur");
                }
            break;
            default :  throw new Exception("Demande non valide");
        }
    }else{
        throw new Exception("Probleme de récupération de données.");
    }
}catch(Exception $e) { // S'il y a eu une erreur, alors...
    echo 'Erreur : ' . $e->getMessage();
}
